package com.mfelipe.appchurrascore

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        btnFut.setOnClickListener(){
            val telaFutebol = Intent(this, FutebolActivity::class.java)
            startActivity(telaFutebol)
        }

        btnPebolim.setOnClickListener(){
            val telaPebolim = Intent(this, PebolimActivity::class.java)
            startActivity(telaPebolim)
        }

        btnTruco.setOnClickListener(){
            val telaTruco = Intent(this, TrucoActivity::class.java)
            startActivity(telaTruco)
        }
    }


}
